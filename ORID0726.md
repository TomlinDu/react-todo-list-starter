# Daily Report (2023/07/26) 

## O(Objective): What did we learn today? What activities did you do? What scenes have impressed you? 
> **React Router**: React Router is a declarative, component-based client and server routing library for React that keeps UI and URL synchronized, has a simple API and powerful features. 
>
> react-router: provides core routing functions for React applications. 
>
> react-router-dom: Based on react-router, it adds some functions in the browser running environment..

> **Axios**: axios is a promise-based HTTP client for browsers and nodejs.

> **Promise:** The promise is an asynchronous solution. Before ES2015, javascript implemented asyncism mainly through callback functions, that is, passing a function into a function to achieve asynchronous operations.

> **Ant Design**: Ant Design is an enterprise UI design language and React component library

##  R(Reflective): Please use one word to express your feelings about today’s class. 

> fulfilling!

##  I(Interpretive): What do you think about this? What was the most meaningful aspect of this activity? 
> I find it fun to write front-end styles, but I'm not familiar with many CSS properties and it takes me a while to get the style I want.

##  D(Decisional): Where do you most want to apply what you have learned today? What changes will you make? 
> I still need to practice writing CSS styles.