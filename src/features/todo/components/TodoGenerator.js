import { useState } from "react";
import { useTodos } from "../../hooks/useTodos";
import { Button } from 'antd';


export default function TodoGenerator() {
    const [taskName, setTaskName] = useState("");
    const { createTodo } = useTodos();

    const handleTaskNameChange = (e) => {
        const value = e.target.value;
        setTaskName(value);
    }
    const handleAddTodoTask = async () => {
        await createTodo({ name: taskName, done: false });
    }
    return (
        <div className='todo-generator'>
            <input placeholder='input a new todo here...' onChange={handleTaskNameChange} value={taskName}></input>
            <Button type="primary" ghost onClick={handleAddTodoTask} disabled={!taskName} > Add </Button>
        </div>
    );
}