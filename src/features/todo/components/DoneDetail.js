import { useSelector } from "react-redux";
import { useParams } from "react-router"

export default function DoneDetail() {
    const { id } = useParams();
    const todoTask = useSelector(state => state.todo.tasks).find(task => task.id === id);
    return (
        <div className="todo-detail">
            <h1>Done Detail</h1>
            <div>{todoTask?.id}</div>
            <div>{todoTask?.name}</div>
        </div>
    )
}