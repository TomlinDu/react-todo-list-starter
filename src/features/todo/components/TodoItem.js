import { useTodos } from "../../hooks/useTodos";
import { EditOutlined, DeleteOutlined, InfoCircleOutlined } from '@ant-design/icons';
import { Modal, Input } from 'antd';
import React, { useState } from 'react';
export default function TodoItem(props) {
    const { task } = props;
    const { deleteTodo, updateTodo, updateTodoText } = useTodos();
    const { TextArea } = Input;
    const [isDetailModalOpen, setIsDetailModalOpen] = useState(false);
    const handleShowDetailButtonClick = () => {
        setIsDetailModalOpen(true);
    }
    const handleDetailOk = () => {
        setIsDetailModalOpen(false);
    }
    const handleDetailCancel = () => {
        setIsDetailModalOpen(false);
    }
    const handleTaskNameClick = async () => {
        await updateTodo(task.id, { done: !task.done });
    }
    const handleRemoveButtonClick = async () => {
        if (window.confirm('Are you sure you wish to delete this item?')) {
            await deleteTodo(task.id);
        }
    }
    const [isEditModalOpen, setIsEditModalOpen] = useState(false);
    const [text, setText] = useState(task.name);
    const handleUpdateTextButtonClick = () => {
        setText(task.name);
        setIsEditModalOpen(true);
    }
    const changeText = (e) => {
        setText(e.target.value);
    };
    const handleEditOk = async () => {
        if (text.trim() === "") {
            alert("文本不能为空");
            return;
        }
        await updateTodoText(task.id, { name: text });
        setIsEditModalOpen(false);
    };
    const handleEditCancel = () => {
        setIsEditModalOpen(false);
    };
    return (
        <div className='todo-item'>
            <div className={`task-name ${task.done ? 'done' : ''}`}
                onClick={handleTaskNameClick}>{task.name}
            </div>
            <div className="buttons-box">
                <div className="showDetail-button" onClick={handleShowDetailButtonClick}>
                    <InfoCircleOutlined />
                </div>
                <div className="updateText-button" onClick={handleUpdateTextButtonClick}>
                    <EditOutlined />
                </div>
                <div className='remove-button' onClick={handleRemoveButtonClick}>
                    <DeleteOutlined />
                </div>
            </div>
            <Modal title="Change Todo's names" open={isEditModalOpen} onOk={handleEditOk} onCancel={handleEditCancel}>
                <TextArea rows={4} maxLength={100} value={text} onChange={changeText} />
            </Modal>
            <Modal title="Details" open={isDetailModalOpen} onOk={handleDetailOk} onCancel={handleDetailCancel}>
                <p>id: {task.id}</p>
                <p>name: {task.name}</p>
            </Modal>
        </div>
    );
}