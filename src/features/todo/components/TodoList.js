import TodoGenerator from "./TodoGenerator";
import TodoGroup from "./TodoGroup";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useTodos } from "../../hooks/useTodos";

export default function TodoList() {
    const dispatch = useDispatch();
    const { loadTodos } = useTodos();
    useEffect(() => { loadTodos() }, [dispatch])

    return (
        <div className='todo-list'>
            <h1>Todo List</h1>
            <TodoGroup />
            <TodoGenerator />
        </div>
    );
}