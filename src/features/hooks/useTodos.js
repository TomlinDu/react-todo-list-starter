import { useDispatch } from "react-redux";
import * as todoApi from "../../apis/todo"
import { initTodoTasks } from "../todo/reducers/todoSlice";
export const useTodos = () => {
    const dispatch = useDispatch();
    async function loadTodos() {
        const response = await todoApi.getTodoTasks();
        dispatch(initTodoTasks(response.data));
    }
    const updateTodo = async (id, todoTask) => {
        await todoApi.updateTodoTask(id, todoTask);
        await loadTodos();
    }
    const deleteTodo = async (id) => {
        await todoApi.deleteTodoTask(id);
        await loadTodos();
    }
    const createTodo = async (todoTask) => {
        await todoApi.addTodoTask(todoTask);
        await loadTodos();
    }
    const updateTodoText = async (id, todoTask) => {
        await todoApi.updateTodoTask(id, todoTask);
        await loadTodos();
    }
    return {
        loadTodos,
        updateTodo,
        deleteTodo,
        createTodo,
        updateTodoText,
    }
}